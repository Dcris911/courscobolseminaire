      ******************************************************************
      * Author: David Gilbert
      * Date: 20 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *     Exemple de comment creer un tableau en deux dimensions
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. ARRAY-DIMMENSION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *        Tableau de deux dimensions
           01 TABLEAU.
               02 RANGEE OCCURS 4 TIMES.
                   05 COLONNE PIC 99 OCCURS 5 TIMES.

           77 I PIC 9.
           77 J PIC 9.
           77 K PIC 99.

       PROCEDURE DIVISION.
           SET K TO 0.
       MAIN-PROCEDURE.
      *        Boucle dans le tableau et assigne une valeur
            PERFORM BOUCLE-FONCTION VARYING I FROM 1 BY 1 UNTIL I > 4
               AFTER J FROM 1 BY 1 UNTIL J > 5.
            DISPLAY TABLEAU
            DISPLAY RANGEE(1)
            DISPLAY RANGEE(2).
            STOP RUN.

       BOUCLE-FONCTION.
           MOVE K TO COLONNE(I,J)
           ADD 1 TO K.

       END PROGRAM ARRAY-DIMMENSION.
