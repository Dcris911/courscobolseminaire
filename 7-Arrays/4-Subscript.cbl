      ******************************************************************
      * Author: David Gilbert
      * Date: 20 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. SUBSCRIPT.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           01 TABLEAU.
               05 VALEUR PIC X(2) OCCURS 5 TIMES.
           77 I PIC 9.
           77 SUBSCRIPT PIC 9.
       PROCEDURE DIVISION.
           MOVE 1 TO SUBSCRIPT.
            SET SUBSCRIPT TO 1.
            SET I TO 0.
            PERFORM BOUCLE UNTIL SUBSCRIPT > 5.
            DISPLAY TABLEAU.
            STOP RUN.
       BOUCLE.
           MOVE I TO VALEUR(SUBSCRIPT)
           SET SUBSCRIPT UP BY 1.
           SET I UP BY 1.

       END PROGRAM SUBSCRIPT.
