      ******************************************************************
      * Author: David Gilbert
      * Date: 21 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. ALTER-TABLE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY "MAIN"
           GO TO PARAGRAPHE2.

       PARAGRAPHE1.
           DISPLAY "PARAGRAPHE1".
           ALTER PARAGRAPHE2 TO PROCEED TO PARAGRAPHE4.
           GO TO MAIN-PROCEDURE.

       PARAGRAPHE2.
           GO TO PARAGRAPHE3.

       PARAGRAPHE3.
           DISPLAY "PARAGRAPHE3".
           GO TO PARAGRAPHE1.

       PARAGRAPHE4.
           DISPLAY "PARAGRAPHE4"
           STOP RUN.

       END PROGRAM ALTER-TABLE.
