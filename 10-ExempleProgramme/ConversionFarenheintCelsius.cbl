      ******************************************************************
      * Author: David Gilbert
      * Date: 21 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Programme servant a convertir des degrees Farenheit en
      *       celsius et vice-versa
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. FARENHEIT-CELSIUS.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           77 TEMPERATURE          PIC S999    VALUES 0.
           77 CHOIXTYPETEMPERATURE PIC X.
           77 RESULTAT             PIC S999V99 VALUES 0.

       PROCEDURE DIVISION.

       MAIN-PROCEDURE.
            PERFORM INTRODUCTION
            IF CHOIXTYPETEMPERATURE = 'C'
                PERFORM CONVERSIONCELSIUS
            ELSE
               IF CHOIXTYPETEMPERATURE = 'F'
                   PERFORM CONVERSIONFARENHEINT
               ELSE
                   DISPLAY "Mettre F ou C"
                   GO TO MAIN-PROCEDURE
               END-IF
            END-IF.

       INTRODUCTION.
           DISPLAY "Entrer la temperature a convertir"
           ACCEPT TEMPERATURE.
           DISPLAY "Entrer le type actuelle de la temperature (F ou C)"
           ACCEPT CHOIXTYPETEMPERATURE.

       CONVERSIONFARENHEINT.
           COMPUTE RESULTAT = TEMPERATURE - 32.
           COMPUTE RESULTAT ROUNDED = RESULTAT * 5.
           COMPUTE RESULTAT ROUNDED = RESULTAT / 9.
           DISPLAY "Farenheint : " TEMPERATURE.
           DISPLAY "Celsius    : " RESULTAT.
           STOP RUN.

       CONVERSIONCELSIUS.
           COMPUTE RESULTAT ROUNDED = TEMPERATURE * 9.
           COMPUTE RESULTAT ROUNDED = RESULTAT / 5.
           COMPUTE RESULTAT = RESULTAT + 32.
           DISPLAY "Celsius    : " TEMPERATURE.
           DISPLAY "Farenheint : " RESULTAT.
           STOP RUN.

       END PROGRAM FARENHEIT-CELSIUS.
