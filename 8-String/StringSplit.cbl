      ******************************************************************
      * Author: David Gilbert
      * Date: 21 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Permet de separer une chaine en plusieurs chaines de
      *      caracteres
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. SPLIT.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           77 STRING1 PIC X(15).
           77 STRING2 PIC X(15).
           77 STRING3 PIC X(10).

           77 STRING4 PIC X(50).
           77 EXEMPLEPOINTER PIC 9(2).
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
            MOVE 'APPRENONS.LA BASE DU.COBOL' TO STRING4.
            UNSTRING STRING4 DELIMITED BY '.' INTO
               STRING1
               STRING2
               STRING3
            END-UNSTRING.

            DISPLAY STRING4
            DISPLAY STRING1
            DISPLAY STRING2
            DISPLAY STRING3.
            STOP RUN.
       END PROGRAM SPLIT.
