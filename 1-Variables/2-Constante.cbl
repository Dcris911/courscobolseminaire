      ******************************************************************
      * Author: David Gilbert
      * Date: 17 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONSTANTE.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *     Une variable normale
           01 UNE-VARIABLE PIC X(15).
      *     Une constante (Literal) format numerique
           01 UN-LITERAL   PIC 9(03) VALUES 123.
      *     Une constante (Literal) format Alphabetique
           01 DEUX-LITERAL PIC X(18) VALUES "Bonjour".
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY UNE-VARIABLE.

      *     QUOTES est une Constante Figurative/Predefini
           MOVE QUOTES TO UNE-VARIABLE
           DISPLAY UNE-VARIABLE.
           MOVE ZERO TO UNE-VARIABLE
           DISPLAY UNE-VARIABLE.

           DISPLAY UN-LITERAL.
           DISPLAY DEUX-LITERAL.
           STOP RUN.
       END PROGRAM CONSTANTE.
