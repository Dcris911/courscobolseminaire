      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *     Equivalent a une boucle FOR en Java
      ******************************************************************
      *    Syntaxe :
      *    PERFORM paragraphe VARYING index FROM initial
      *      BY incrementation UNTIL condition
      *        code
      *    END-PERFORM
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. BOUCLE-FOR.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           77 A PIC 9.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           PERFORM FONCTION-1 VARYING A FROM 0 BY 4 UNTIL A > 6
           DISPLAY "Sorti!"
           STOP RUN.

       FONCTION-1.
           DISPLAY A.
       END PROGRAM BOUCLE-FOR.
