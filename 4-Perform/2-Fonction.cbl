      ******************************************************************
      * Author: David Gilbert
      * Date: 18 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *     Equivalent a un appel de fonction en Java
      ******************************************************************
      *    Syntaxe :
      *    PERFORM paragraphe [nombreDeFois TIMES].
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. FONCTION-PERFORM.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           77 A PIC 9.
       PROCEDURE DIVISION.
           SET A TO 0.

       MAIN-PROCEDURE.
           DISPLAY "A:" A.
           PERFORM FONCTION-1.
           PERFORM FONCTION-2 2 TIMES.
           DISPLAY "A:" A.
           STOP RUN.

       FONCTION-1.
           ADD 1 TO A.
           DISPLAY "Bonjour le monde".
      *    FONCTION-1 FINI ICI
       FONCTION-2.
           ADD 1 TO A.
      *    FONCTION-2 FINI ICI
       END PROGRAM FONCTION-PERFORM.
