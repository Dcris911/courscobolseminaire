      ******************************************************************
      * Author: David Gilbert
      * Date: 17 mai 2018
      * Purpose: Seminaire
      * Tectonics: cobc
      ******************************************************************
      *    Syntaxe :
      *    PERFORM number-of-times TIMES
      *        CODE
      *    END-PERFORM
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. BOUCLE-PERFORM.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
           77 X PIC 9.
       PROCEDURE DIVISION.
           SET X TO 0.
       MAIN-PROCEDURE.
           PERFORM 3 TIMES
               ADD 1 TO X GIVING X
               DISPLAY "X="X
           END-PERFORM
            STOP RUN.
       END PROGRAM BOUCLE-PERFORM.
